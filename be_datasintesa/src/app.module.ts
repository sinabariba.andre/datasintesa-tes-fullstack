import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { CsvModule } from 'nest-csv-parser';
import { GraphModule } from './graph/graph.module';

@Module({
  imports: [
    GraphModule,
    MongooseModule.forRoot('mongodb://127.0.0.1:27017/nest'),
    MulterModule.register({
      dest: '../uploads',
    }),
    CsvModule,
  ],
})
export class AppModule {}
