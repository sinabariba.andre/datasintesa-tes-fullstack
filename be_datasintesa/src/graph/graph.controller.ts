import csv from 'csvtojson';
import { parse } from 'papaparse';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { Express } from 'express';
import { GraphService } from './graph.service';
import { CreateGraphDto } from './dto/create-graph.dto';
import { UpdateGraphDto } from './dto/update-graph.dto';
import { FileInterceptor } from '@nestjs/platform-express';
@Controller('graph')
export class GraphController {
  constructor(private readonly graphService: GraphService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file: Express.Multer.File) {
    const fileCsv = file.buffer.toString();

    const parseFile = parse(fileCsv, {
      header: true,
      skipEmptyLines: true,
      complete: (result) => result.data,
    });

    const data = parseFile.data;
    return await this.graphService.upload('sa', 'b', 'c', 'd', data);
    // return { field, data };

    // return fileCsv;
  }

  @Get()
  findAll() {
    return this.graphService.findAll();
  }
}
