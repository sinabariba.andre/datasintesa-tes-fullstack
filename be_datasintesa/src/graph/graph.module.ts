import { Module } from '@nestjs/common';
import { GraphService } from './graph.service';
import { GraphController } from './graph.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Graph, GraphSchema } from 'src/schemas/graph.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Graph.name, schema: GraphSchema }]),
  ],
  controllers: [GraphController],
  providers: [GraphService],
})
export class GraphModule {}
