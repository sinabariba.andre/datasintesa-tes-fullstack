import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Graph, GraphDocument } from 'src/schemas/graph.schema';
import { CreateGraphDto } from './dto/create-graph.dto';
import { UpdateGraphDto } from './dto/update-graph.dto';

@Injectable()
export class GraphService {
  constructor(
    @InjectModel(Graph.name) private graphModel: Model<GraphDocument>,
  ) {}

  async upload(
    resultTime: string,
    enodebId: string,
    cellId: string,
    availDur: string,
    rawdata: any,
  ) {
    console.log('');

    const uploadFile = new this.graphModel({
      resultTime: 'as',
      enodebId: 'sas',
      cellId: 'asa',
      availDur: 'sa',
      rawdata,
    });
    return await uploadFile.save();
  }

  async create(createGraphDto: CreateGraphDto): Promise<Graph> {
    const createdGraph = new this.graphModel(createGraphDto);
    return await createdGraph.save();
  }

  async findAll(): Promise<Graph[]> {
    return this.graphModel.find().exec();
  }

  findOne(id: number) {
    return `This action returns a #${id} graph`;
  }

  update(id: number, updateGraphDto: UpdateGraphDto) {
    return `This action updates a #${id} graph`;
  }

  remove(id: number) {
    return `This action removes a #${id} graph`;
  }
}
