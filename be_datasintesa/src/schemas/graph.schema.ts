import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as scm } from 'mongoose';

export type GraphDocument = Graph & Document;

@Schema({ collection: 'raw_data' })
export class Graph extends Document {
  // @Prop()
  // 'Granularity Period': string;
  // @Prop()
  // 'L.Cell.Avail.Dur': string;
  // @Prop()
  // 'L.Cell.Unavail.Dur.EnergySaving': string;
  // @Prop()
  // 'L.Cell.Unavail.Dur.Manual': string;
  // @Prop()
  // 'L.Cell.Unavail.Dur.Sys': string;
  // @Prop()
  // 'L.Cell.Unavail.Dur.Sys.S1Fail': string;
  // @Prop()
  // 'L.Voice.E2EVQI.Accept.Times': string;
  // @Prop()
  // 'L.Voice.E2EVQI.Bad.Times': string;
  // @Prop()
  // 'L.Voice.E2EVQI.Excellent.Times': string;
  // @Prop()
  // 'L.Voice.E2EVQI.Good.Times': string;
  // @Prop()
  // 'L.Voice.E2EVQI.Poor.Times': string;
  // @Prop()
  // 'Object Name': string;
  // @Prop()
  // 'Reliability': string;
  // @Prop()
  // 'Result Time': string;

  @Prop()
  resultTime: string;
  @Prop()
  enodebId: string;
  @Prop()
  cellId: string;
  @Prop()
  availDur: string;
  @Prop()
  rawdata: scm.Types.Map;
  mapOfString: {
    type: scm.Types.Map;
    of: string;
  };
}

export const GraphSchema = SchemaFactory.createForClass(Graph);
