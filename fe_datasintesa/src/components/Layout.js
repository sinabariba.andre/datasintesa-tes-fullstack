import React from "react";

const Layout = ({ children }) => {
  return (
    <div className="w-screen h-auto flex flex-col ">
      <main className="w-full bg-[#eaeaea] min-h-[100vh]">{children}</main>
    </div>
  );
};

export default Layout;
