import axios from "axios";
import React, { useState } from "react";
import { MdCloudUpload } from "react-icons/md";
const Upload = () => {
  const [csvAsset, setCsvAsset] = useState(null);
  const [msg, setMsg] = useState(null);

  const uploadFile = (e) => {
    const csvFile = e.target.files[0];
    setCsvAsset(csvFile);
  };

  const submitFile = (e) => {
    console.log(csvAsset);
    e.preventDefault();
    const formData = new FormData();
    formData.append("file", csvAsset);

    axios
      .post("http://localhost:5000/graph", formData)
      .then((res) => {
        console.log("File Upload success");
        console.log(res.data);
      })
      .catch((err) => console.log("File Upload Error"));
  };
  return (
    <div className="w-full min-h-screen flex items-center flex-col   justify-center">
      <h2 className="text-xl py-6 font-semibold">Upload files</h2>
      <form className="w-[90%] md:w-[75%] border border-emerald-300  rounded-lg p-4 flex flex-col items-center justify-center gap-4">
        <div className="group flex justify-center items-center flex-col border-2 border-dotted border-gray-300 w-full h-225 md:h-420 cursor-pointer rounded-lg">
          <label className="w-full h-full flex flex-col items-center justify-center cursor-pointer">
            <div className="w-full h-full flex flex-col items-center justify-center gap-2">
              <MdCloudUpload className="text-gray-500 text-3xl hover:text-emerald-700" />
              <p className="text-textColor">Click here to upload file</p>
            </div>
            <input
              type="file"
              name="uploadFile"
              accept="file/.csv"
              onChange={uploadFile}
              className="w-0 h-0"
            />
          </label>
        </div>
        <div className="flex items-center w-full">
          <button
            type="submit"
            className=" w-full m-auto md:w-auto flex  items-center justify-center border-none outline-none bg-emerald-500 px-12 py-1 rounded-lg text-lg text-white font-semibold"
            onClick={(e) => submitFile(e)}
          >
            Upload
          </button>
        </div>
      </form>
    </div>
  );
};

export default Upload;
